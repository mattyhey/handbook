---
title: "Globalization & Localization"
summary: Lead Gitlab’s go-to-market localization strategy, operations and technology platform, with a primary focus on Sales, Marketing, Growth & E-commerce Product areas.
---

## Levels

### Senior Manager, Globalization Technology

The Senior Manager, Globalization Technology reports to the Director, Globalization & Localization.

#### Senior Manager, Globalization Technology Job Grade

The Senior Manager, Globalization Technology is a [grade 9](https://about.gitlab.com/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades) role.

#### Senior Manager, Globalization Technology Role

The Senior Manager, Globalization Technology is passionate about the changing localization technology landscape and can build a program that leans on tried-and-true tools and practices but with a sharp eye toward the future, especially capabilities that are disruptive in nature and in alignment with the future of automation and AI-assisted tooling. They are a recognized leader in the localization space with a background in building systems that allow for seamless automation, integration, and data capture. They have a recognized talent for helping businesses go global and a demonstrable track record for solutions that solve perennial localization challenges and turning a nascent program into a well-oiled machine. A strong candidate for this role is comfortable with ambiguity and able to navigate within a rapidly transforming environment.

#### Senior Manager, Globalization Technology Responsibilities

- Drive the development and evolution of our localization infrastructure to enable the translation of diverse offerings, including web collateral, product strings, support documentation, and in-language copywriting endeavors.
- Oversee our technology roadmap by ensuring we meet critical deadlines, show demonstrable improvements at scale and efficiency, and directly impact our deliverability for multiple teams and stakeholders of localized content.
- Implement integrations with content and product systems to build a holistic ecosystem that allows for the successful localization of any surface area while helping teams and suppliers maintain quality and time-to-market expectations.
- Partner closely with development teams, content managers, and stakeholders to ensure alignment on workstreams and success measurements
- Establish operational performance metrics and define a reporting framework for ongoing communication and analysis with an constant eye toward cost and efficiency improvement.
- Work with developers and suppliers to set up AI-driven routing, workflow enhancements with machine translation, natural language processing, and machine learning to solve localization scale and time-to-market challenges.
- Be a constant learner, develop a deep understanding of the changing tech landscape in localization and ensure GitLab is adopting or building the best-in-class solutions.
- Lead by example and communicate localization best practices throughout the organization.

#### Senior Manager, Globalization Technology Desirable Skills, Knowledge, and Experience

- Experience building and leading the development of translation and localization technology stacks.
- Demonstrable success with building functional relationships with internal stakeholders and external suppliers to support localization endeavors.
- Deep understanding of localization operational data and reporting frameworks that demonstrate the overall health, strengths, and weaknesses of translation pipelines.
- Vast working knowledge of the latest localization products and services with a critical eye toward industry trends.
- Experience brining cross-functional teams together to accomplish global expansion goals.
- Effective at communicating the business impact of localization operations throughout a globally distributed organization.
- Strong communication skills.
- Fluency in English.
- You share our values, and work in accordance with those values.

#### Career Ladder

The next step in the Senior Manager, Globalization Technology job family is not yet defined at GitLab.

#### Performance Indicators

- Build and maintain a state-of-the-art localization platform in partnership with internal development teams and external suppliers.
- Deploye solutions for localization workflows that rely more on integrated ML and generative AI tooling.
- Speed and throughput of translation; % of on-time deliveries.
- Localization Quality; average language quality scores.
- Total localization costs relative to content volume / format; fully-loaded price per word.

### Director, Globalization & Localization

The Director, Globalization & Localization reports to the VP Marketing Strategy & Platforms.

#### Director, Globalization & Localization Job Grade

The Director, Globalization & Localization is a [grade 10](https://about.gitlab.com/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades) role.

#### Director, Globalization & Localization Role

The Director, Globalization & Localization will lead Gitlab’s go-to-market localization strategy, operations and technology platform, with a primary focus on Sales, Marketing, Growth & E-commerce Product areas. You’ll apply your deep domain knowledge and experience in web & product internationalization, localization lifecycle management, translation management system, machine translation and international expansion to help share the GitLab story & value across languages and cultures. You will translate, localize and customize market-appropriate content that resonates with the target audience. You’ll also work with product and engineering to ensure our globalization infrastructure and technology platform can support both current and future use cases. The qualified candidate will be a driven and inspiring leader who has a successful track record of building and leading a global team, including agencies and linguists network, and is comfortable being a player coach while scaling the globalization & localization function as our company grows.

#### Director, Globalization & Localization Responsibilities

- Lead the go-to-market localization lifecycle management, operations, and technology infrastructure initiatives and teams
- Recruit, inspire, develop and retain a globally distributed all-remote team. Manage the localization agencies and linguists network to ensure continuous improvement of quality and productivity.
- Develop and maintain strong working relationships with culturally and geographically diverse stakeholders. Collaborate with sales & marketing regional leaders.
- Define and execute on an end-to-end localization and transcreation programs from planning, budgeting to delivery and QA.
- Help drive significant improvement in international SEO and revenue growth through special cross-functional globalization initiatives
- Allocate resources and budget to efficiently maximize business outcomes, and contribute to the strategic objectives along the customer journey

#### Director, Globalization & Localization Desirable Skills, Knowledge, and Experience

- Progressive experience leading globalization / localization at fast growing technology companies preferably with Software-as-a-Service offerings and strong open source communities
- Leadership experience managing a high-performing team in a distributed environment, with an emphasis on people leadership, building teams to scale, and engaging global and local stakeholders.
- Experience collaborating across multiple internal departments and teams, building effective relationships and influencing change
- Strong network and relationship with localization agencies, project managers and linguists
- Deep domain expertise in localization lifecycle, CAT tools, TMS. Technical understanding of common DevOps platforms, CMS, and marketing automation systems. - Demonstrated ability to learn and leverage new technologies
- Professional fluency in English and at least one of the following languages: French, Germany, Spanish, or Japanese
- Comfortable using data to measure results and inform decision making and strategy development.
- You share our values, and work in accordance with those values.

#### Career Ladder

The next step in the Director, Globalization & Localization job family is not yet defined at GitLab.

#### Performance Indicators

- Speed and throughput of translation; % of on-time deliveries
- Localization Quality; average language quality scores
- Total localization costs relative to content volume / format; fully-loaded price per word
- Non-English SEO lift; % uplift in organic traffic and conversions for target markets
- New pipeline lift; % uplift in Marketing Qualified Leads & Sales Accepted Opportunities for target markets

#### Hiring Process

Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find their job title on our [team page](https://about.gitlab.com/company/team/).

- Qualified candidates will be invited to schedule a 30 minute [screening call](https://about.gitlab.com/handbook/hiring/talent-acquisition-framework/req-overview/#screening) with one of our Global Recruiters.
- A 45 minute interview with our VP, Marketing Strategy & Platforms
- A 45 minute interview with our Director, Digital Experience or Group Product Manager, Growth
- A 45 minute interview with our Director, Marketing Operations or Manager, Marketing Operations
- A 45 minute interview with our VP, Integrated Marketing or Director, Global Field Marketing
- A 30 minute interview with our VP, Field Operations or VP, Sales Strategy & Analytics
- A 30 minute interview with our Chief Marketing Officer or VP, Product Marketing
- A 45 minute interview with our Director, People Business Partner

Additional details about our process can be found on our [hiring page](https://about.gitlab.com/handbook/hiring/).
